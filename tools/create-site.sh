#!/bin/bash
read -p "Site folder name: " NAME
read -p "SSH username: " SSH_NAME
read -p "SSH passowrd: " PASSWORD


read -p "MYSQL root password: " MYSQL_ROOT_PASSWORD
read -p "MYSQL username: " MYSQL_USER
read -p "MYSQL password: " MYSQL_PASSWORD
read -p "MYSQL database: " MYSQL_DATABASE


echo "Create folder code: "

docker exec -i manager bash -c "mkdir -vp '/custom_home/${NAME}';mkdir -vp '/custom_home/${NAME}/public_html'"
docker exec -i manager bash -c "cat /custom_etc/passwd > /etc/passwd"
docker exec -i manager bash -c "cat /custom_etc/group > /etc/group"
docker exec -i manager bash -c "cat /custom_etc/shadow > /etc/shadow"
docker exec -i manager bash -c "useradd --shell /bin/bash --home-dir /home/${NAME} ${SSH_NAME}"
docker exec -i manager bash -c "echo '${SSH_NAME}:${PASSWORD}' | chpasswd ''"
docker exec -i manager bash -c "cat /etc/passwd > /custom_etc/passwd"
docker exec -i manager bash -c "cat /etc/group > /custom_etc/group"
docker exec -i manager bash -c "cat /etc/shadow > /custom_etc/shadow"
docker exec -i manager bash -c "chown -R ${SSH_NAME}:${SSH_NAME} /custom_home/${NAME}"

echo "Create database: ${MYSQL_DATABASE}"
docker exec mariadb mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE} ;
GRANT ALL PRIVILEGES ON ${MYSQL_DATABASE}.* TO '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';"
